package com.a.introducction.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GildedRoseRefactoredTest {

    public static final int EXPIRED_SELLIN = -1;
    public static final int NOT_EXPIRED_SELLIN = 15;
    public static final int DEFAULT_QUALITY = 3;
    public static final String DEFAULT_ITEM = "DEFAULT_ITEM";
    public static final String AGED_BRIE = "Aged Brie";
    public static final int MAXIMUM_QUALITY = 50;
    private static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    private static final int SELLIN_GREATER_THAN_10 = 15;
    private static final int SELLIN_BETWEEN_5_AND_10 = 7;
    private static final int POSITIVE_SELLIN_LESS_THAN_5 = 3;

    @Test
    void unexpiredDefaultItem_QualityDecreasesBy1() {
        //Arrange
        GildedRose app =
                createGildedRoseWithOneItem(DEFAULT_ITEM,
                        NOT_EXPIRED_SELLIN, DEFAULT_QUALITY);

        //Act
        app.updateQuality();

        //Assertions
        Item expected = new Item(DEFAULT_ITEM,
                NOT_EXPIRED_SELLIN - 1, DEFAULT_QUALITY - 1);

        assertItem(expected, app.items[0]);
    }

    @Test
    void expiredDefaultItem_qualityDecreasedBy2() {
        //Arrange
        GildedRose app =
                createGildedRoseWithOneItem(DEFAULT_ITEM,
                        EXPIRED_SELLIN, DEFAULT_QUALITY);

        //Act
        app.updateQuality();

        //Assertions
        Item expected = new Item(DEFAULT_ITEM,
                EXPIRED_SELLIN - 1, DEFAULT_QUALITY - 2);

        assertItem(expected, app.items[0]);
    }

    @Test
    void unexpiredAgedBrie_qualityIncreasesBy1() {
        //Arrange
        GildedRose app =
                createGildedRoseWithOneItem(AGED_BRIE,
                        NOT_EXPIRED_SELLIN, DEFAULT_QUALITY);

        //Act
        app.updateQuality();

        //Assertions
        Item expected = new Item(AGED_BRIE,
                NOT_EXPIRED_SELLIN - 1, DEFAULT_QUALITY + 1);

        assertItem(expected, app.items[0]);
    }

    @Test
    void expiredAgedBrie_qualityIncreasesBy2() {
        //Arrange
        GildedRose app =
                createGildedRoseWithOneItem(AGED_BRIE,
                        EXPIRED_SELLIN, DEFAULT_QUALITY);

        //Act
        app.updateQuality();

        //Assertions
        Item expected = new Item(AGED_BRIE,
                EXPIRED_SELLIN - 1, DEFAULT_QUALITY + 2);

        assertItem(expected, app.items[0]);
    }

    @Test
    void unexpiredAgedBrie_qualityDoesNotGoBeyondMaximum() {
        //Arrange
        GildedRose app =
                createGildedRoseWithOneItem(AGED_BRIE,
                        NOT_EXPIRED_SELLIN, MAXIMUM_QUALITY);

        //Act
        app.updateQuality();

        //Assertions
        Item expected = new Item(AGED_BRIE,
                NOT_EXPIRED_SELLIN - 1, MAXIMUM_QUALITY);

        assertItem(expected, app.items[0]);
    }

    @Test
    public void backStagePassesBeyond10Days_qualityIncreasesBy1() {

        GildedRose app = createGildedRoseWithOneItem(BACKSTAGE_PASSES,
                SELLIN_GREATER_THAN_10, DEFAULT_QUALITY);

        app.updateQuality();

        Item expected = new Item(BACKSTAGE_PASSES,
                SELLIN_GREATER_THAN_10 - 1, DEFAULT_QUALITY + 1);

        assertItem(expected, app.items[0]);
    }

    @Test
    public void backStageBetween5And10Days_qualityIncreasesBy2() {

        GildedRose app = createGildedRoseWithOneItem(BACKSTAGE_PASSES,
                SELLIN_BETWEEN_5_AND_10, DEFAULT_QUALITY);

        app.updateQuality();

        Item expected = new Item(BACKSTAGE_PASSES,
                SELLIN_BETWEEN_5_AND_10 - 1, DEFAULT_QUALITY + 2);

        assertItem(expected, app.items[0]);
    }

    @Test
    public void backStageLessThan5Days_qualityIncreasesBy3() {

        GildedRose app = createGildedRoseWithOneItem(BACKSTAGE_PASSES,
                POSITIVE_SELLIN_LESS_THAN_5, DEFAULT_QUALITY);

        app.updateQuality();

        Item expected = new Item(BACKSTAGE_PASSES,
                POSITIVE_SELLIN_LESS_THAN_5 - 1, DEFAULT_QUALITY + 3);

        assertItem(expected, app.items[0]);
    }


    private void assertItem(Item expected, Item actual) {
        assertEquals(expected.name, actual.name);
        assertEquals(expected.sellIn, actual.sellIn);
        assertEquals(expected.quality, actual.quality);
    }

    private GildedRose createGildedRoseWithOneItem(String idemType, int sellIn, int quality) {
        Item item = new Item(idemType, sellIn, quality);
        Item[] items = new Item[]{item};
        GildedRose app = new GildedRose(items);
        return app;
    }
}